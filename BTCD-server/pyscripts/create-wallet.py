from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException
import logging, sys

print "--------------------------------"
print "Creating new account & wallet"
print "--------------------------------"
#
#
#
if len(sys.argv) == 1:
    print "ERROR: Please provide a wallet name!"
else:
    print "Account Name:"
    walletName = sys.argv[1:]
    print(walletName)

    #logging.basicConfig()
    #logging.getLogger("BitcoinRPC").setLevel(logging.DEBUG)

    rpc_user = "bitcoinrpc"
    rpc_password = "PASSWORD_HERE"
    rpc_connection = AuthServiceProxy("http://%s:%s@127.0.0.1:8332"%(rpc_user, rpc_password))

    new_wallet_address_generated = rpc_connection.getnewaddress("%s"%(walletName))
    print "Wallet Address:"
    print(new_wallet_address_generated)
#
#
#
print "--------------------------------"
print "Finished"
print "--------------------------------"