from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException
import logging, json

print "--------------------------------"
print "Getting accounts"
print "--------------------------------"

#logging.basicConfig()
#logging.getLogger("BitcoinRPC").setLevel(logging.DEBUG)

rpc_user = "bitcoinrpc"
rpc_password = "PASSWORD_HERE"
rpc_connection = AuthServiceProxy("http://%s:%s@127.0.0.1:8332"%(rpc_user, rpc_password))

account_list = rpc_connection.listaccounts()
account_list_total_count = len(account_list)

print "Account List:"
print 'RAW:', account_list
print 'TOTAL:', account_list_total_count

#print '[0] ---> ', account_list[0]
print "..."
for index in range(0, account_list_total_count):
    print "raw element ---> %d" % (index)
print "..."
# List comprehension.
#ascii_list = [ x.encode('ascii') for x in account_list ]
#print 'ASCII:', ascii_list

#data_string = json.dumps(account_list)
#print 'JSON:', data_string

names_list = [ item.encode('ascii') for item in account_list ]
print "item List:"
print 'RAW:', names_list

addresses_list = [ str(rpc_connection.getaddressesbyaccount(item)) for item in account_list ]
print "Addresses List:"
print 'RAW:', addresses_list

balances_list = [ "%f"%(rpc_connection.getbalance(item)) for item in account_list ]
print "Balances List:"
print 'RAW:', balances_list

#block_times = [ block for block in account_list ]
#print(block_times)

#for val in rpc_connection.listaccounts():
#    print(val)

print "--------------------------------"
print "Finished"
print "--------------------------------"