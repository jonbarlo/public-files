from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException
import logging

print "--------------------------------"
print "Getting server info"
print "--------------------------------"

logging.basicConfig()
logging.getLogger("BitcoinRPC").setLevel(logging.DEBUG)

rpc_user = "bitcoinrpc"
rpc_password = "PASSWORD_HERE"
rpc_connection = AuthServiceProxy("http://%s:%s@127.0.0.1:8332"%(rpc_user, rpc_password))
print(rpc_connection.getinfo())

print "--------------------------------"
print "Finished"
print "--------------------------------"